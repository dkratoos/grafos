import { createRouter, createWebHistory } from 'vue-router'
import Menu from '../modules/menu/Menu.vue'
import Game from '../modules/game/Game.vue'
import Config from '../modules/config/Config.vue'
import Ranking from '../modules/ranking/Ranking.vue'
import Tutorial from '../modules/tutorial/Tutorial.vue'
import Team from '../modules/team/Team.vue'

const routes = [
  {
    path: '/',
    name: 'Menu',
    component: Menu
  },
  {
    path: '/game',
    name: 'Game',
    component: Game
  },
  {
    path: '/config',
    name: 'Config',
    component: Config
  },
  {
    path: '/ranking',
    name: 'Ranking',
    component: Ranking
  },
  {
    path: '/tutorial',
    name: 'Tutorial',
    component: Tutorial
  },
  {
    path: '/team',
    name: 'Team',
    component: Team
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

function depthFirstSearch(tree = [], visited = []) {
  if (tree.length === 0) {
    return visited
  }
  let node = tree.shift()
  if (visited.indexOf(node) === -1) {
    visited.push(node)
    tree.unshift(...node.children)
  }
  return depthFirstSearch(tree, visited)
}

export default (tree) => {
  tree = JSON.parse(JSON.stringify(tree))
  return depthFirstSearch(tree, [])
}
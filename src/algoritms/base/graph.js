import utils from '../../assets/js/utils'

function Vertice(ctx, x, y, w, cor, text) {
  this.vizinhos = function (v, val) {
    new Aresta(this.ctx, this, v, val)
  }

  this.setColor = function (cor) {
    ctx.beginPath()
    ctx.arc(x, y, w + 5, 0, Math.PI * 2)
    ctx.closePath()
    ctx.fillStyle = cor
    ctx.fill()
  }

  this.id = utils.uuid()
  this.ctx = ctx
  this.ctx.beginPath()
  this.x = x
  this.y = y
  this.w = w
  this.text = text
  this.circle = new Path2D()

  this.circle.arc(this.x, this.y, this.w, 0, Math.PI * 2)

  if (cor) {
    ctx.fillStyle = cor
    ctx.fill(this.circle)
  }

  new RotuloVertice(this.ctx, this, this.text)
  this.ctx.closePath()
}

function Aresta(ctx, v1, v2, text) {
  this.ctx = ctx
  this.ctx.beginPath()
  this.v1 = v1
  this.v2 = v2
  this.text = text
  this.ctx.strokeStyle = '#fff'
  this.ctx.globalCompositeOperation = 'destination-over'
  this.ctx.moveTo(this.v1.x, this.v1.y)
  this.ctx.lineTo(
    this.v2.x,
    this.v2.y
  )
  this.ctx.stroke()
  new RotuloAresta(this.ctx, this, this.text)
  this.ctx.closePath()
}

function RotuloVertice(ctx, v, text) {
  this.ctx = ctx
  this.ctx.beginPath()
  this.v = v
  this.text = text
  this.x = (this.v.x - text.length * 2.5)
  this.y = this.v.y + this.v.w + 20
  this.ctx.fillStyle = '#fff'
  this.ctx.font = '18px Arial'
  this.ctx.fillText(this.text, this.x, this.y)
  this.ctx.closePath()
}

function RotuloAresta(ctx, a, text) {
  this.ctx = ctx
  this.ctx.beginPath()
  this.a = a
  let maxX = Math.max(this.a.v1.x, this.a.v2.x)
  let minX = Math.min(this.a.v1.x, this.a.v2.x)
  let maxY = Math.max(this.a.v1.y, this.a.v2.y)
  let minY = Math.min(this.a.v1.y, this.a.v2.y)
  let x = (maxX + minX) / 2
  let y = (maxY + minY) / 2
  this.ctx.fillStyle = 'red'
  this.ctx.font = '18px monospace'
  this.ctx.fillText(text, x, y)
  this.ctx.closePath()
}

export {
  Vertice,
  Aresta,
  RotuloVertice,
  RotuloAresta,
}

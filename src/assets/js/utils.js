export default {
  random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  },

  randomLetter() {
    return String.fromCharCode(this.random(65, 90))
  },

  removeClass(target, className) {
    const elements = document.querySelectorAll(target)

    for (let i = 0;i < elements.length;i++) {
      elements[i].classList.remove(className)
    }
  },

  uuid() {
    let dt = new Date().getTime()
    let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      let r = (dt + Math.random() * 16) % 16 | 0
      dt = Math.floor(dt / 16)
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16)
    })
    return uuid
  }
}
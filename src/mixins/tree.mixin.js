import utils from '../assets/js/utils'

export default {
  data() {
    return {
      answer: [],
      graph: []
    };
  },

  created () {
    this.generateTree();
  },

  methods: {
    undo () {
      const node = this.answer.pop();

      if (!node) {
        return;
      }

      const element = document.getElementById(node.id);

      element.classList.remove('active');
    },

    generateTree() {
      const LIMIT_CHILDREN = 5;
      const randomNumber = utils.random(1, LIMIT_CHILDREN);

      this.graph = [
        {
          id: utils.uuid(),
          name: utils.randomLetter().toUpperCase() + LIMIT_CHILDREN,
          children: this.generate(randomNumber),
        },
      ]
    },

    generate (limit) {
      const list = [];

      if (limit === 0) {
        return list;
      }

      for (let index = 0; index < limit; index++) {
        const newLimit = utils.random(0, limit)

        list.push({
          id: utils.uuid(),
          name: utils.randomLetter().toUpperCase() + newLimit,
          children: this.generate(newLimit - 1)
        });
      }

      return list
    },

    clearReply () {
      this.answer = [];
      utils.removeClass('.tf-tree .active', 'active');
    },

    addAnswer (node) {
      if (!node.id) {
        return;
      }

      document.getElementById(node.id).classList.add('active');

      const isExist = this.answer.some(item => item.id === node.id);

      if (isExist) {
        return
      }

      this.answer.push({
        id: node.id
      });
    },
  }
}
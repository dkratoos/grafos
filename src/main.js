import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import TreeItem from './components/TreeItem.vue'
import Modal from './components/Modal.vue'
import './assets/css/styles.scss'
import '@fortawesome/fontawesome-free/css/all.min.css'

createApp(App)
  .component('TreeItem', TreeItem)
  .component('Modal', Modal)
  .use(router)
  .mount('#app')


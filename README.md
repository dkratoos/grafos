# GraphGame
O objetivo deste projeto é para desenvolver o aprendizado em grafos e seus algoritmos

- Existirão jogos dos algoritimos e o usuário deverá passar por eles, para aprender a resolver o problema.
- A cada jogada o usuário receberá feedback sobre o resultado da jogada.
- O usuário poderá jogar até que o seu grafo seja completo.

## Possíveis Jogos:
- BFS: é um algoritmo de busca em grafos utilizado para realizar uma busca ou travessia num grafo e estrutura de dados do tipo árvore.
- DFS: é um algoritmo de busca em grafos utilizado para realizar uma busca ou travessia num grafo e estrutura de dados do tipo pilha.
- Dijkstra: é um algoritmo de busca em grafos utilizado para encontrar o menor caminho entre dois vértices.
- Prim: é um algoritmo de busca em grafos utilizado para encontrar o menor caminho entre dois vértices.


## Instalação
```
npm install
```

### Compilar hot-reloads para desenvolvimento
```
npm run serve
```

### Compilar e minificar para produção
```
npm run build
```

### Lint
```
npm run lint
```
